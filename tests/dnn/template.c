#include "generated_params.h"
#include "address.h"
#include "mmio.h"
#include "run_layer.h"
#include "traps.h"

int main() {
    void* params[4];
    dnn_params_t params_type[4];

    enable_interrupts();

    reg_write32(INPUT_BASE_ADDRESS, SRAM_BASE);
    reg_write32(BIAS_BASE_ADDRESS, SRAM_BASE);
    reg_write32(OUTPUT_BASE_ADDRESS, SRAM_BASE);
    reg_write32(WEIGHTS_BASE_ADDRESS, SRAM_BASE);
    reg_write32(VECTOR0_BASE_ADDRESS, SRAM_BASE);
    reg_write32(VECTOR1_BASE_ADDRESS, SRAM_BASE);
    reg_write32(VECTOR2_BASE_ADDRESS, SRAM_BASE);

#define {LAYER}
#ifndef fc
    params[0] = &{LAYER}_matrixParams_0;
    params_type[0] = MATRIX;
    params[1] = &{LAYER}_vectorParams_0;
    params[2] = &{LAYER}_vectorInstructionConfig_0;
    params_type[1] = VECTOR;
    params_type[2] = VECTOR;
    run_layer(params, params_type, 3);
#else
    params[0] = &fc_vectorParams_0;
    params[1] = &fc_vectorInstructionConfig_0;
    params_type[0] = VECTOR;
    params_type[1] = VECTOR;
    run_layer(params, params_type, 2);
#endif
}
