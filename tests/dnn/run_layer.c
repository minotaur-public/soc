#include "run_layer.h"

#include <stdio.h>
#include <string.h>

#include "address.h"
#include "mmio.h"

void sendSerializedParams(void *params, int width, uintptr_t address) {
  int *ptr = params;

  // round up to multiple of int
  int paddedWidth = ((width + 32 - 1) / 32) * 32;

  for (int i = 0; i < paddedWidth / 32; i++) {
    reg_write32(address, *ptr);
    ptr++;
  }
}

void sendMatrixParams(void *matrixParams) {
  sendSerializedParams(matrixParams, MatrixParams_width, MATRIX_PARAMS_IN);
  // printf("MatrixParams written\n");
}

void sendVectorParams(void *vectorParams, void *vectorInstructions) {
  sendSerializedParams(vectorParams, VectorParams_width, VECTOR_PARAMS_IN);
  // printf("VectorParams written\n");

  sendSerializedParams(vectorInstructions, VectorInstructionConfig_width,
                       VECTOR_PARAMS_IN);
  // printf("VectorInstructions written\n");
}

__attribute__((noinline)) void wait_for_minotaur() {
  __asm__ volatile(
      "mino_wait_loop:"
      "wfi\n"
      //"j mino_wait_loop\n"
      "nop\n"
      "nop\n"
      "nop\n");
}

void run_layer(void **params, dnn_params_t *params_type, int count) {
  for (int iteration = 0; iteration < 1; iteration++) {
    printf("Writing params...\n");
    for (int i = 0; i < count; i++) {
      if (params_type[i] == MATRIX) {
        sendMatrixParams(params[i]);
      } else if (params_type[i] == VECTOR) {
        sendVectorParams(params[i], params[i + 1]);
        i++;

        wait_for_minotaur();
      }
    }

    printf("Operation finished!\n");
  }

  printf("Runtimes:\n");
  printf("Matrix Unit: %lu cycles\n", reg_read64(MATRIXUNIT_CYCLE_COUNT));
  printf("Vector Unit: %lu cycles\n", reg_read64(VECTORUNIT_CYCLE_COUNT));
  printf("DNN Accelerator: %lu cycles\n", reg_read64(ACC_CYCLE_COUNT));

  for (volatile int dummy = 0; dummy < 0; dummy++) {
  }
}
