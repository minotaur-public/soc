#ifndef RUN_LAYER_H
#define RUN_LAYER_H

#include "dnn_params.h"
#include "mmio.h"

typedef enum { MATRIX, VECTOR } dnn_params_t;

void run_layer(void **params, dnn_params_t *params_type, int count);

#endif
