#ifndef ACCELERATOR_PARAMS_H
#define ACCELERATOR_PARAMS_H

static const unsigned int MatrixParams_width = 380;
static const unsigned int VectorParams_width = 520;
static const unsigned int VectorInstructionConfig_width = 659;
#endif
