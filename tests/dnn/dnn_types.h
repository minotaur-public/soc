#ifndef DNN_TYPES_H
#define DNN_TYPES_H

#include <stdbool.h>

#ifndef SOC_COSIM
typedef struct {
  int INPUT_OFFSET;
  int WEIGHT_OFFSET;
  int OUTPUT_OFFSET;
  bool WEIGHT_TRANSPOSE;

  // tiling
  int loops[2][6];
  int inputXLoopIndex[2];
  int inputYLoopIndex[2];
  int reductionLoopIndex[2];
  int weightLoopIndex[2];
  int fxIndex;
  int fyIndex;
  int weightReuseIndex[2];
  int STRIDE;
  bool REPLICATION;

  bool RELU;
  bool BIAS;
  int BIAS_OFFSET;
  bool RESIDUAL;
  int RESIDUAL_OFFSET;

  // pooling
  bool MAXPOOL;
  bool AVGPOOL;

  bool WEIGHT;

  bool STORE_IN_ACC;
  bool ACC_FROM_ACC;

  // special vector ops
  bool SOFTMAX;
  bool ATTENTION_MASK;
  bool ATTENTION_SCALING;
  bool FC;
  bool NO_NORM;

  bool SOFTMAX_GRAD;
  bool FC_GRAD;
  bool NO_NORM_GRAD;
  bool RELU_GRAD;
  bool BIAS_GRAD;
  bool CROSS_ENTROPY_GRAD;
  bool MSE_GRAD;
  bool BCE_WITH_LOGITS_GRAD;

  // permutation ops
  bool INPUT_TRANSPOSE;
  bool CONCAT_INPUT;
  bool CONCAT_WEIGHT;
  bool SPLIT_OUTPUT;

  bool GRAD_CLIPPING;
  bool GRAD_CLIPPING_UNIT_TEST;

  bool WEIGHT_SPLITTING;
  int WEIGHT_GRADIENT_OFFSET;
  int BIAS_GRADIENT_OFFSET;
  float learningRate;

  bool ACC_T_INPUT;
  bool ACC_T_WEIGHT;
  bool ACC_T_OUTPUT;

  int inputExpBias;
  int weightExpBias;
  int outputExpBias;
  int residualExpBias;
} SimplifiedParams;
#endif

typedef struct __attribute__((packed, aligned(4))) {
  int INPUT_OFFSET;
  int WEIGHT_OFFSET;

  int loops[2][6];
  int inputXLoopIndex[2];
  int inputYLoopIndex[2];
  int reductionLoopIndex[2];
  int weightLoopIndex[2];
  int fxIndex;
  int fyIndex;
  int weightReuseIndex[2];

  // weight address generator loop
  int weightAddressGenLoops[2][5];
  // in the inner loop, there are actually 2 reduction loops: the
  // standard reduction loop and the reduction that is parallelized in
  // the systolic array
  int weightAddressGenReductionLoopIndex[2];
  int weightAddressGenWeightLoopIndex[2];
  int weightAddressGenFxIndex;
  int weightAddressGenFyIndex;
  int weightAddressGenInputXLoopIndex;  // only care about outer X and Y loops
  int weightAddressGenInputYLoopIndex;

  int STRIDE;
  int HEAD_SIZE_LG2;

  bool WEIGHT_TRANSPOSE : 1;
  bool REPLICATION : 1;

  bool STORE_IN_ACC : 1;
  bool ACC_FROM_ACC : 1;
  bool CONCAT_INPUT : 1;
  bool CONCAT_HEAD_WEIGHTS : 1;
  bool TRANPOSE_INPUTS : 1;
} MatrixParams;

static const unsigned int MatrixParams_width =
    3 * 32 /* OFFSETS */ + (12 + 10) * 10 /* Loops */ +
      (6 + 3) * 2 * 3 /* Loop indices */ + 8 * 1 /* Bools */ + 2 + 8;

typedef struct __attribute__((packed)) {
  // 3 address generators:
  // - Vector Input
  // - Residual/Op0Src1
  // - Bias/Op3Src1

  // Address Gen 0 (vector input)
  int VECTOR_OFFSET;
  int addressGen0Loop[3];  // 2d tensor

  // Address Gen 1 (residual/op0src1)
  int ADDRESS_GEN1_OFFSET;
  int addressGen1Loops[2][3];
  int addressGen1InputXLoopIndex[2];
  int addressGen1InputYLoopIndex[2];
  int addressGen1WeightLoopIndex[2];

  // Address Gen 2 (bias/op3src1)
  int ADDRESS_GEN2_OFFSET;
  int addressGen2Loops[2][3];
  int addressGen2InputXLoopIndex[2];
  int addressGen2InputYLoopIndex[2];
  int addressGen2WeightLoopIndex[2];

  int VECTOR_OUTPUT_OFFSET;
  int SCALAR_OUTPUT_OFFSET;

  int scalarOutputCount;

  int outputLoops[2][3];
  int outputXLoopIndex[2];
  int outputYLoopIndex[2];
  int outputWeightLoopIndex[2];
  int FULL_HEAD_SIZE;
  bool SPLIT_HEAD : 1;

  bool addressGen0Enable : 1;
  int addressGen1Mode : 2;  // 1- residual, 2- 2dtensor
  int addressGen2Mode : 2;  // 1- bias, 2- 2dtensor
  bool MAXPOOL : 1;
  bool AVGPOOL : 1;
} VectorParams;

static const unsigned int VectorParams_width =
    5 * 32 /* OFFSETS */ + 4 * 6 * 10 /* Loops */ +
      3 * 6 * 3 /* Loop indices */ + 9 * 1 /* Bools */ + 10 + 2 * 2;

typedef struct __attribute__((packed)) {
  int instType : 2;
  // static const unsigned int vector = 0;
  // static const unsigned int reduction = 1;
  // static const unsigned int accumulation = 2;

  int vInput : 2;
  // static const unsigned int readFromSystolicArray = 1;
  // static const unsigned int readFromVectorFetch = 2;
  // static const unsigned int readFromAccumulation = 3;

  int vAccumulatePush : 1;

  // src0 refers to lhs and src1 refers to rhs

  // Stage 0: add, mult
  int vOp0Src1 : 1;
  // static const unsigned int readInterface = 1;
  int vOp0 : 2;  // add, sub, mult
  // static const unsigned int nop = 0;
  // static const unsigned int vadd = 1;
  // static const unsigned int vmult = 2;
  // static const unsigned int vsub = 3;

  // Stage 1: exp
  int vOp1 : 1;
  // static const unsigned int vexp = 1;

  // Stage 2: send to reduce unit
  int vOp2 : 1;
  // static const unsigned int toReduce = 1;

  // Stage 3: add, div
  int vOp3Src0 : 1;  // use existing or read from reduce interface
  int vOp3Src1 : 2;  // don't read, read from reduce interface 2, or
                     // normal interface
  // static const unsigned int readReduceInterface = 1;
  // static const unsigned int readNormalInterface = 2;
  int vOp3 : 2;  // add, div
  // static const unsigned int vadd = 1;
  // static const unsigned int vdiv = 2;

  // Stage 4: relu
  int vOp4 : 1;
  // static const unsigned int vrelu = 1;

  // Vector Unit write out
  int vDest : 1;
  // static const unsigned int vWriteOut = 1;

  int rCount : 10;
  int rOp : 2;
  // static const unsigned int radd = 1;
  // static const unsigned int rmax = 2;

  int rDuplicate : 1;

  int rDest : 2;
  // static const unsigned int toVectorSrc0 = 1;
  // static const unsigned int toVectorSrc1 = 2;
  // static const unsigned int sWriteOut = 3;

  // static const unsigned int width = 38;

} VectorInstructions;

static const unsigned int VectorInstructions_vector = 0;
static const unsigned int VectorInstructions_reduction = 1;
static const unsigned int VectorInstructions_accumulation = 2;
static const unsigned int VectorInstructions_readFromSystolicArray = 1;
static const unsigned int VectorInstructions_readFromVectorFetch = 2;
static const unsigned int VectorInstructions_readFromAccumulation = 3;
static const unsigned int VectorInstructions_readInterface = 1;
static const unsigned int VectorInstructions_nop = 0;
static const unsigned int VectorInstructions_vadd = 1;
static const unsigned int VectorInstructions_vmult = 2;
static const unsigned int VectorInstructions_vsub = 3;
static const unsigned int VectorInstructions_vexp = 1;
static const unsigned int VectorInstructions_toReduce = 1;
static const unsigned int VectorInstructions_readReduceInterface = 1;
static const unsigned int VectorInstructions_readNormalInterface = 2;
static const unsigned int VectorInstructions_vdiv = 2;
static const unsigned int VectorInstructions_vrelu = 1;
static const unsigned int VectorInstructions_vWriteOut = 1;
static const unsigned int VectorInstructions_radd = 1;
static const unsigned int VectorInstructions_rmax = 2;
static const unsigned int VectorInstructions_toVectorSrc0 = 1;
static const unsigned int VectorInstructions_toVectorSrc1 = 2;
static const unsigned int VectorInstructions_sWriteOut = 3;
static const unsigned int VectorInstructions_width = 59;

typedef struct __attribute__((packed)) {
  VectorInstructions inst[8];
  int instCount[8];
  int instLen;
  int instLoopCount;
} VectorInstructionConfig;

static const unsigned int VectorInstructionConfig_width =
    VectorInstructions_width * 8 + 20 * 8 + 3 + 10;

#ifndef SOC_COSIM
#ifndef FINETUNING
typedef enum { SRAM, RRAM } MemorySource;

typedef struct {
  MemorySource inputs;
  MemorySource weights;
  MemorySource bias;
  MemorySource residual;
  MemorySource outputs;
} MemoryMap;
#endif
#endif

#endif
