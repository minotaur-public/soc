#ifndef __MMIO_H__
#define __MMIO_H__

#include <stdint.h>

typedef struct{
	uint64_t val1;
	uint64_t val2;
} uint128_t;

static inline void reg_write8(uintptr_t addr, uint8_t data)
{
	volatile uint8_t *ptr = (volatile uint8_t *) addr;
	*ptr = data;
}

static inline uint8_t reg_read8(uintptr_t addr)
{
	volatile uint8_t *ptr = (volatile uint8_t *) addr;
	return *ptr;
}

static inline void reg_write16(uintptr_t addr, uint16_t data)
{
	volatile uint16_t *ptr = (volatile uint16_t *) addr;
	*ptr = data;
}

static inline uint16_t reg_read16(uintptr_t addr)
{
	volatile uint16_t *ptr = (volatile uint16_t *) addr;
	return *ptr;
}

static inline void reg_write32(uintptr_t addr, uint32_t data)
{
	volatile uint32_t *ptr = (volatile uint32_t *) addr;
	*ptr = data;
}

static inline uint32_t reg_read32(uintptr_t addr)
{
	volatile uint32_t *ptr = (volatile uint32_t *) addr;
	return *ptr;
}

static inline void reg_write64(unsigned long addr, uint64_t data)
{
	volatile uint64_t *ptr = (volatile uint64_t *) addr;
	*ptr = data;
}

static inline uint64_t reg_read64(unsigned long addr)
{
	volatile uint64_t *ptr = (volatile uint64_t *) addr;
	return *ptr;
}

// Name: reg_write128
// 
// Description: Writes a 128 bit value to an address; acts like
//              two 64 bit writes to consecutive address spaces
//
// Arguments:   addr - address to write to; passed data will be written
//                     both to addr and the next unsigned long addr
//              data - 128 bit value to write
//
static inline void reg_write128(unsigned long addr, uint128_t data)
{

    volatile uint64_t *ptr = (volatile uint64_t *) addr;      // cast to 64 bit pointer 
    *ptr = data.val1;                                // write first half of value
    *(ptr+1) = data.val2;

}

// Name: reg_read128
// 
// Description: Writes a 128 bit value to an address; acts like
//              two 64 bit writes to consecutive address spaces
//
// Arguments:   addr - address to write to; passed data will be written
//                     both to addr and the next unsigned long addr
//              data - 128 bit value to write
//

static inline uint128_t reg_read128(unsigned long addr){

    volatile uint64_t *ptr = (volatile uint64_t *) addr;

    uint64_t val1 = *ptr;                 // read first half at address
    uint64_t val2 = *(ptr+1);                 // read second half
    uint128_t readvalue = {val1, val2};   // assemble into uint128 struct

    return readvalue;
}

// name: compare128
// 
// Description: equality operatory for uint128_t. Function returns 1
//              if both fields of two 128 bit structs are equivalent
//              and 0 otherwise

static inline int compare128(uint128_t a, uint128_t b){
    if (a.val1 == b.val1 && a.val2 == b.val2) {
        return 1;
    }

    return 0;
}

#endif
