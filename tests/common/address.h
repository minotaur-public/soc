#ifndef __ADDRESS_H__
#define __ADDRESS_H__

#include "rram_address.h"

#define PLIC_REG 0xc000000
#define MSIP_REG 0x2000000

// *********************    SRAM addresses   ***************************
#define CHIP_PMU (0x13000000 + 0x100)
#define IRRAM_POC_H_START (0x13000000 + 0x104)
#define IRRAM_POC_H_POWER_DOWN (0x13000000 + 0x108)
#define CHIP_POWER_GOOD (0x13000000 + 0x10c)

// *********************    SRAM addresses   ***************************
// Start memory address for each SRAM bank
// First 0x10000 of SRAM is used as instructions
#define TLSRAM_MMIO_BASE 0x4000000  // start of SRAM MMIO
#define TLSRAM_MMIO_OFFSET 0x100
#define SRAM_BASE 0x3000000
#define SRAM_BANK_OFFSET 0x040000
#define SRAM_POWER_DOWN 0xff  // constant to power down the SRAM

#define SRAM_PD_REG TLSRAM_MMMIO + 0x0  // sram power down register

// *********************    Accelerator addresses   ***************************
// Accelerator register addresses
#define MATRIX_PARAMS_IN 0x29000
#define INPUT_BASE_ADDRESS 0x29004
#define WEIGHTS_BASE_ADDRESS 0x29008
#define GRADIENT_BASE_ADDRESS 0x2900c

#define VECTOR_PARAMS_IN 0x29100
#define VECTOR0_BASE_ADDRESS 0x29104
#define VECTOR1_BASE_ADDRESS 0x29108
#define VECTOR2_BASE_ADDRESS 0x2910c
#define OUTPUT_BASE_ADDRESS 0x29110
#define SCALAR_BASE_ADDRESS 0x29114

#define MATRIXUNIT_CYCLE_COUNT 0x29200
#define VECTORUNIT_CYCLE_COUNT 0x29208
#define ACC_CYCLE_COUNT 0x29210
#define ACC_POWER_MODE 0x29220
#define ACC_POWER_GOOD 0x29230

// *********************    C2C addresses   ***************************
// C2C addresses
// DMA
#define DMA_0 0x12000000
#define DMA_1 0x12000100
#define DMA_2 0x12000200
#define DMA_3 0x12000300

#define DMA_SOURCE_ADDR_OFFSET 0x0
#define DMA_DEST_ADDR_OFFSET 0x10
#define DMA_LEN_OFFSET 0x20
#define DMA_START_OFFSET 0x30

// C2C
#define C2C_0 0x9000000
#define C2C_1 0x9000100
#define C2C_2 0x9000200
#define C2C_3 0x9000300

#define C2C_ADDRESS_1 0x9000000
#define C2C_ADDRESS_2 0xA000000
#define C2C_PIN_ADDRESS 0x11000000

// C2C mmio register absolute addresses
#define C2C_MMIO_RRAM_ADDRESS 0x00 + C2C_PIN_ADDRESS
#define C2C_MMIO_SRAM_ADDRESS 0x04 + C2C_PIN_ADDRESS
#define C2C_MMIO_BOOT_MAP 0x08 + C2C_PIN_ADDRESS
#define C2C_MMIO_SEND_DONE 0x0C + C2C_PIN_ADDRESS
#define C2C_MMIO_RECV_START 0x10 + C2C_PIN_ADDRESS

// *********************    Additional constants   ***************************
#define SRAM_WORDS 32768  // size of SRAM in words
#define BYTES_PER_ROW \
  16  // bytes per row of memory bank - addresses to skip
      // between consecutive memory accesses
#define NUM_ACCESSES \
  8                // required number of consecutive writes -
                   // must load a multiple of 64 times to trigger a write
#define SILENT -1  // flag for suppressing prints in r/w_chunk128

#define INTERLEAVE 1     // interleave RRAM accesses between banks
#define SINGLE_BANK 2    // make all accesses in the same bank
#define NUM_RRAM_BANK 2  // number of RRAM banks

#endif  // _ADDRESS_H_
