#include "traps.h"

#include <stdint.h>
#include <stdio.h>
#include "syscall.h"

// Enable interrupts by setting the correct CPU registers.
void enable_interrupts() {
  // Clear exisiting machine interrupts
  reg_write8(MSIP_REG, 0);

  // Clear existing supervisor interrupt
  clear_csr(mip, MIP_SEIP);

  // Enable supervisor + machine interrupts
  set_csr(mstatus, MSTATUS_SIE | MSTATUS_MIE);
  set_csr(mie, MIP_SEIP | MIP_MEIP);

  // Configure the interrupt on the PLIC
  reg_write32(PLIC_REG + 0x2000, 0xFFFF);  // enable
  reg_write8(PLIC_REG + 0x4, 0xF);     // priority Minotaur
  reg_write8(PLIC_REG + 0x8, 0xE);     // priority AXI4Vision
  reg_write8(PLIC_REG + 0xc, 0xD);     // priority Fisheye
  reg_write8(PLIC_REG + 0x10, 0xC);     // priority Optical Flow
  reg_write8(PLIC_REG + 0x14, 0xB);     // priority IMU
  reg_write8(PLIC_REG + 0x200000, 0);  // threshold
}

enum InterruptNumber {
    PLACEHOLDER_INTERRUPT,
    DNN_INTERRUPT,
    FISHEYE_INTERRUPT,
    OPTICALFLOW_INTERRUPT,
    //RANSAC_INTERRUPT,
    HISTEQUALIZE_INTERRUPT,
    GAUSS_SCHARR_INTERRUPT,
    GRID_FAST_INTERRUPT,
    //IMU_INTERRUPT,
    FISHEYE2_INTERRUPT,
    OPTICALFLOW2_INTERRUPT,
    //RANSAC2_INTERRUPT,
    HISTEQUALIZE2_INTERRUPT,
    GAUSS_SCHARR2_INTERRUPT,
    GRID_FAST2_INTERRUPT
};


void __attribute__((weak, interrupt)) handle_trap(void) {
    uint64_t interrupt = read_csr(mcause);
    if (interrupt >> 63 == 1) {
        uint64_t id = (interrupt << 1) >> 1; // clear top bit

        if (id == 3) {
            // Clear interrupt
            reg_write8(MSIP_REG, 0);

            printf("Software Interrupt Received.\n");

            return;
       } else if (id == 11) {
            // Claim the interrupt
            const uint64_t claim_number = reg_read32(PLIC_REG + 0x200000 + 4);

            switch (claim_number) {
                case DNN_INTERRUPT:
                    printf("Received DNN interrupt\n");
                    break;
                case FISHEYE_INTERRUPT:
                    printf("Received Fisheye interrupt\n");
                    break;
                case OPTICALFLOW_INTERRUPT:
                    printf("Received Optical Flow interrupt\n");
                    break;
                //case RANSAC_INTERRUPT:
                //    printf("Received RANSAC interrupt\n");
                //    break;
                case HISTEQUALIZE_INTERRUPT:
                    printf("Received Histequalize interrupt\n");
                    break;
                case GAUSS_SCHARR_INTERRUPT:
                    printf("Received Gauss-Scharr interrupt\n");
                    break;
                case GRID_FAST_INTERRUPT:
                    printf("Received Grid FAST interrupt\n");
                    break;
                //case IMU_INTERRUPT:
                //    printf("Received IMU interrupt\n");
                //    break;
                case FISHEYE2_INTERRUPT:
                    printf("Received Fisheye2 interrupt\n");
                    break;
                case OPTICALFLOW2_INTERRUPT:
                    printf("Received Optical Flow2 interrupt\n");
                    break;
                //case RANSAC2_INTERRUPT:
                //    printf("Received RANSAC2 interrupt\n");
                //    break;
                case HISTEQUALIZE2_INTERRUPT:
                    printf("Received Histequalize2 interrupt\n");
                    break;
                case GAUSS_SCHARR2_INTERRUPT:
                    printf("Received Gauss-Scharr2 interrupt\n");
                    break;
                case GRID_FAST2_INTERRUPT:
                    printf("Received Grid FAST2 interrupt\n");
                    break;
                default:
                    printf("Unknown claim number %ld\n", claim_number);
            }

            // Mark it as claimed
            reg_write32(PLIC_REG + 0x200000 + 4, claim_number);

           return;
        } else {
            printf("Unknown Interrupt: %lu\n", id);
        }
    } else {
        //printf("Exception! Cause: %lu PC: %lu\n", cause, epc);
        printf("Exception!");
        uintptr_t faulting_address = read_csr(mtval);
        printf("Faulting Address: %lu\n", faulting_address);
    }

     _exit(1);
}
