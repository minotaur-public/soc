#ifndef TRAPS_H
#define TRAPS_H

#include "address.h"
#include "encoding.h"
#include "mmio.h"

#include <stdio.h>

void enable_interrupts();

void __attribute__((weak, interrupt)) handle_trap(void);

#endif
