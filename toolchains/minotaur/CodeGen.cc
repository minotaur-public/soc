#include <systemc.h>

#include <experimental/filesystem>
#include <fstream>
#include <iostream>
#include <string>

#include "src/AccelTypes.h"
#include "src/Params.h"
#include "src/TypeToBits.h"
#include "test/generic/Generic.h"
#include "test/mobilebert/MobileBERT.h"
#include "test/resnet/ResNet.h"
#include "test/toolchain/MapOperation.h"

std::vector<char> HexToBytes(const std::string &hex) {
  std::vector<char> bytes;

  for (int i = hex.length() - 1; i >= 1; i -= 2) {
    std::string byteString = hex.substr(i - 1, 2);
    char byte = (char)strtol(byteString.c_str(), NULL, 16);
    bytes.push_back(byte);
  }

  return bytes;
}

template <typename T>
std::vector<char> ConvertParamsToBytes(T &params) {
  std::string hexString = TypeToBits(params).to_string(SC_HEX);

  // strip out 0x
  hexString = hexString.substr(2, std::string::npos);
  // pad with 0 in case of odd length
  hexString = "0" + hexString;

  return HexToBytes(hexString);
}

template <typename T>
T ConvertBytesToParams(std::string hexString) {
  sc_lv<Wrapped<T>::width> bits;
  bits.assign_from_string(hexString);
  return BitsToType<T>(bits);
}

void WriteBytesToFile(const std::string &file, std::vector<char> bytes) {
  std::ofstream outputFile(file, std::ios::out | std::ios::binary);
  outputFile.write((char *)bytes.data(), bytes.size());
  outputFile.close();
}

template <typename T>
void WriteParam(const std::string basePath, const std::string &file,
                const std::string &headerFile, T &params) {
  std::vector<char> bytes = ConvertParamsToBytes(params);
  WriteBytesToFile(basePath + file, bytes);

  std::string xxdCmd =
      "cd " + basePath + " && xxd -i " + file + " >> " + headerFile;
  std::system(xxdCmd.c_str());
  //   remove(file.c_str());
}

void WriteCFileHeader(std::ofstream &cFile, const std::string &paramsPath) {
  cFile << "#include <stddef.h>\n\n";
  cFile << "#include \"" << paramsPath << "\"\n";
  cFile << "#include \"address.h\"\n";
  cFile << "#include \"mmio.h\"\n";
  cFile << "#include \"run_layer.h\"\n\n";
  cFile << "#include \"traps.h\"\n\n";

  cFile << "int main() {\n";
  cFile << "\tvoid* params[4];\n";
  cFile << "\tdnn_params_t params_type[4];\n";

  cFile << "\tenable_interrupts();\n";
}

void WriteCFileBody(std::ofstream &cFile, const std::string &layer,
                    std::deque<BaseParams *> opParams,
                    const SimplifiedParams &simplifiedParams,
                    const MemoryMap &memoryMap) {
  cFile << std::endl;
  cFile << "\treg_write32(INPUT_BASE_ADDRESS, SRAM_BASE);\n";
  cFile << "\treg_write32(OUTPUT_BASE_ADDRESS, SRAM_BASE);\n";
  cFile << "\treg_write32(WEIGHTS_BASE_ADDRESS, SRAM_BASE);\n";
  cFile << "\treg_write32(BIAS_BASE_ADDRESS, SRAM_BASE);\n";
  cFile << "\treg_write32(VECTOR0_BASE_ADDRESS, SRAM_BASE);\n";
  cFile << "\treg_write32(VECTOR1_BASE_ADDRESS, SRAM_BASE);\n";
  cFile << "\treg_write32(VECTOR2_BASE_ADDRESS, SRAM_BASE);\n";

  cFile << std::endl;

  int paramCount = opParams.size();

  int matrixParamIndex = 0;
  int vectorParamIndex = 0;
  int paramIndex = 0;
  while (opParams.size() > 0) {
    bool matrixParamsValid, vectorParamsValid;

    BaseParams *baseParam = opParams.front();

    MatrixParams *matrixParams = dynamic_cast<MatrixParams *>(baseParam);
    matrixParamsValid = matrixParams != NULL;

    if (matrixParamsValid) {
      opParams.pop_front();
      baseParam = opParams.front();
    }

    VectorParams *vectorParams = dynamic_cast<VectorParams *>(baseParam);
    VectorInstructionConfig *vectorInstructionConfig;
    vectorParamsValid = vectorParams != NULL;

    if (vectorParamsValid) {
      opParams.pop_front();
      baseParam = opParams.front();

      vectorInstructionConfig =
          dynamic_cast<VectorInstructionConfig *>(baseParam);
      opParams.pop_front();
    }

    if (matrixParamsValid) {
      cFile << "\tparams[" << paramIndex << "] = &" << layer << "_matrixParams_"
            << matrixParamIndex << ";\n";
      cFile << "\tparams_type[" << paramIndex << "] = MATRIX;\n";
      paramIndex++;
      matrixParamIndex++;
    }
    if (vectorParamsValid) {
      cFile << "\tparams[" << paramIndex << "] = &" << layer << "_vectorParams_"
            << vectorParamIndex << ";\n";
      cFile << "\tparams[" << paramIndex + 1 << "] = &" << layer
            << "_vectorInstructionConfig_" << vectorParamIndex << ";\n";

      cFile << "\tparams_type[" << paramIndex << "] = VECTOR;\n";
      cFile << "\tparams_type[" << paramIndex + 1 << "] = VECTOR;\n";

      paramIndex += 2;
      vectorParamIndex++;
    }
  }

  cFile << "\n\trun_layer(params, params_type, " << paramCount << ");\n";
}

void WriteCFileEnd(std::ofstream &cFile) { cFile << "}\n"; }

void WriteCFile(const std::string &basePath, const std::string &paramsPath,
                const std::string &layer, std::deque<BaseParams *> opParams,
                const SimplifiedParams &simplifiedParams,
                const MemoryMap &memoryMap) {
  std::string fileName = basePath + layer + ".c";
  std::ofstream cFile(fileName);

  WriteCFileHeader(cFile, paramsPath);
  WriteCFileBody(cFile, layer, opParams, simplifiedParams, memoryMap);
  WriteCFileEnd(cFile);
}

void WriteParams(const std::string &basePath, const std::string &layer,
                 std::deque<BaseParams *> opParams) {
  int matrixParamIndex = 0;
  int vectorParamIndex = 0;
  while (opParams.size() > 0) {
    bool matrixParamsValid, vectorParamsValid;

    BaseParams *baseParam = opParams.front();

    MatrixParams *matrixParams = dynamic_cast<MatrixParams *>(baseParam);
    matrixParamsValid = matrixParams != NULL;

    if (matrixParamsValid) {
      opParams.pop_front();
      baseParam = opParams.front();
    }

    VectorParams *vectorParams = dynamic_cast<VectorParams *>(baseParam);
    VectorInstructionConfig *vectorInstructionConfig;
    vectorParamsValid = vectorParams != NULL;

    if (vectorParamsValid) {
      opParams.pop_front();
      baseParam = opParams.front();

      vectorInstructionConfig =
          dynamic_cast<VectorInstructionConfig *>(baseParam);
      opParams.pop_front();
    }

    if (matrixParamsValid) {
      WriteParam(basePath,
                 layer + "_matrixParams_" + std::to_string(matrixParamIndex),
                 "generated_params.h", *matrixParams);

      matrixParamIndex++;
    }
    if (vectorParamsValid) {
      WriteParam(basePath,
                 layer + "_vectorParams_" + std::to_string(vectorParamIndex),
                 "generated_params.h", *vectorParams);
      WriteParam(basePath,
                 layer + "_vectorInstructionConfig_" +
                     std::to_string(vectorParamIndex),
                 "generated_params.h", *vectorInstructionConfig);

      vectorParamIndex++;
    }
  }
}

void MapNetwork(Network &network, const std::string &networkName,
                const std::string &folderPrefix,
                const std::string &task = "inference") {
  std::string basePath =
      "../../tests/dnn/networks/" + networkName + "/" + folderPrefix;
  std::experimental::filesystem::create_directories(basePath);

  std::string paramsPath = basePath + "generated_params.h";
  std::string relativeParamsPath =
      "networks/" + networkName + "/" + folderPrefix + "generated_params.h";
  remove(paramsPath.c_str());
  std::system(std::string("touch " + paramsPath).c_str());

  std::vector<Workload> workloads = network.getAllWorkloads();

  if (networkName == "mobilebert" && task == "inference") {
    for (auto &workload : workloads) {
      // these workloads are prefixed with "mobilebert_encoder_layer_0_"
      // remove the prefix
      workload.name = workload.name.substr(27);
    }
  }

  if (networkName == "mobilebert" && task == "backward") {
    MobileBERT &mobilebert = dynamic_cast<MobileBERT &>(network);
    workloads = mobilebert.getFullBackwardPass();
  }

  for (auto &workload : workloads) {
    std::cout << "Mapping " << workload.name << "\n";
    SimplifiedParams params = workload.params;

    std::deque<BaseParams *> opParams;
    std::deque<AcceleratorMemoryMap> dnnMemoryMaps;

    MapOperation(params, workload.memoryMap, opParams, dnnMemoryMaps);

    WriteParams(basePath, workload.name, opParams);

    WriteCFile(basePath, relativeParamsPath, workload.name, opParams, params,
               workload.memoryMap);
  }
}

void WriteParamWidths() {
  std::ofstream headerFile("../../tests/dnn/dnn_params.h");
  headerFile << "#ifndef ACCELERATOR_PARAMS_H\n";
  headerFile << "#define ACCELERATOR_PARAMS_H\n\n";

  headerFile << "static const unsigned int MatrixParams_width = "
             << Wrapped<MatrixParams>::width << ";\n";
  headerFile << "static const unsigned int VectorParams_width = "
             << Wrapped<VectorParams>::width << ";\n";
  headerFile << "static const unsigned int VectorInstructionConfig_width = "
             << Wrapped<VectorInstructionConfig>::width << ";\n";

  headerFile << "#endif\n";
}

int sc_main(int argc, char *argv[]) {
  WriteParamWidths();

  const char *generate_for = std::getenv("GENERATE_FOR");

  bool generate_all =
      generate_for == NULL || std::string(generate_for) == "all";

  // Map ResNet18 (manual mapping)
  if (generate_all || std::string(generate_for) == "resnet18") {
    std::cout << "--- Mapping ResNet18" << std::endl;

    ResNet resnet18("resnet18");
    MapNetwork(resnet18, "resnet18", "/");
  }

  // Map MobileBERT (manual mapping)
  if (generate_all || std::string(generate_for) == "mobilebert") {
    std::cout << "--- Mapping MobileBERT" << std::endl;

    MobileBERT mobilebert("mobilebert", "inference");

    MapNetwork(mobilebert, "mobilebert", "/");
  }

  return 0;
}
