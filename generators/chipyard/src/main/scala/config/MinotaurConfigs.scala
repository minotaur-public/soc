package chipyard

import org.chipsalliance.cde.config.{Config}

import freechips.rocketchip.diplomacy._
import freechips.rocketchip.devices.tilelink._
import freechips.rocketchip.tilelink._
import freechips.rocketchip.devices.debug._
import freechips.rocketchip.rocket._
import freechips.rocketchip.tile._
import freechips.rocketchip.util._

import minotaur.config._
import minotaur._
import testchipip._

// --------------
// Base for Minotaur configs
// --------------
class MinotaurAbstractConfig extends Config(
  new minotaur.config.WithWideBus ++ // make buses 128 bits wide
  new freechips.rocketchip.rocket.WithNBigCores(1) ++         // single rocket-core
  new chipyard.config.AbstractConfig)

class MinotaurBaseConfig extends Config(
        new MinotaurAbstractConfig
  )


// --------------
// Custom Configs
// --------------

// Name:         MinotaurRocketConfig
// Description:  Full Minotaur configuration with RRAM, SRAM, Accelerator
//               and C2C adapter

class GenericMinotaurConfig
    extends Config(
        new minotaur.config.WithGenericVersion ++
        // @param sbusWidth: width of the system bus
        // @param datatype: P8 | E4M3
        // @param systolicArrayDimension: 16 | 32
        new minotaur.WithDNN(sbusWidth=128, datatype="P8", systolicArrayDimension=16, 2, "generic") ++ // add Accelerator
        new minotaur.WithNBanksSRAM512KB(minotaur.A.TLSRAM512KB, 8) ++ 
        new chipyard.harness.WithVerification ++
        // new WithRingFrontBus ++
        new MinotaurBaseConfig
    )


// Name:         GenericSynthesizableMinotaurConfig
//
// Description:  Simulation configuration with C2C bypass fixed
//               and including other modules useful for verification.
class GenericSynthesizableMinotaurConfig
    extends Config(
      // new minotaur.config.WithC2C(4, true) ++    // With C2C bypass fix
        // new chipyard.harness.WithChipIDConfig ++ // ChipID Module
        // new minotaur.config.WithChipIDConfig ++  // ChipID Module
        new freechips.rocketchip.subsystem.WithoutTLMonitors ++
        new GenericMinotaurConfig
    )

// Name:         GenericTapedOutMinotaurConfig
//
// Description:  Simulation configuration that does not include any fixes
//               or additional verification modules. This configuration
//               reflects the chip, as best as possible in simulation,
//               as it was taped out.
class GenericTapedOutMinotaurConfig
    extends Config(
      new freechips.rocketchip.subsystem.WithoutTLMonitors ++
        new GenericMinotaurConfig
    )

