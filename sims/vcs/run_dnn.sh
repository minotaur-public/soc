export NETWORK=$1  # mobileber | resnet18
export TESTS=$2    # see ../../tests/dnn/networks
export DATA_DIR=../../generators/minotaur-blocks/accelerator/binary_data/$1/
export DATATYPE=P8_1
export IC_DIMENSION=16
export OC_DIMENSION=16
export SIMS=customposit,accelerator
# export SPDLOG_LEVEL=debug
make CONFIG=GenericSynthesizableMinotaurConfig DATATYPE=$DATATYPE IC_DIMENSION=$IC_DIMENSION OC_DIMENSION=$OC_DIMENSION verilog
make CONFIG=GenericSynthesizableMinotaurConfig DATATYPE=$DATATYPE IC_DIMENSION=$IC_DIMENSION OC_DIMENSION=$OC_DIMENSION run-binary BINARY=../../tests/dnn/networks/$1/$2.riscv | tee sim.log
