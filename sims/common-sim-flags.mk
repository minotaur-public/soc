#----------------------------------------------------------------------------------------
# common gcc configuration/optimization
#----------------------------------------------------------------------------------------
SIM_OPT_CXXFLAGS := -O3
LRISCV=-lriscv

export USE_CHISEL6=0

SIM_CXXFLAGS = \
	$(CXXFLAGS) \
	$(SIM_OPT_CXXFLAGS) \
	-std=c++17 \
	-I$(RISCV)/include \
	-I$(dramsim_dir) \
	-I$(GEN_COLLATERAL_DIR) \
	-I$(base_dir)/generators/minotaur-blocks/lib/spdlog/include \
	-I$(base_dir)/generators/minotaur-blocks/accelerator \
	-I$(base_dir)/generators/minotaur-blocks/accelerator/test/common \
	-I/cad/mentor/2024.1/Mgc_home/shared/include \
	-D$(DATATYPE) \
	-DIC_DIMENSION=$(IC_DIMENSION) \
	-DOC_DIMENSION=$(OC_DIMENSION) \
	-DNO_SYSC \
	-DNO_UNIVERSAL \
	$(EXTRA_SIM_CXXFLAGS)

ifneq (,$(findstring Generic,$(CONFIG)))
SIM_CXXFLAGS += -DGENERIC_SOC 
endif

SIM_LDFLAGS = \
	$(LDFLAGS) \
	-L$(RISCV)/lib \
	-Wl,-rpath,$(RISCV)/lib \
	-L$(sim_dir) \
	-L$(dramsim_dir) \
	$(LRISCV) \
	-lfesvr \
	-ldramsim \
	-lstdc++fs \
	$(EXTRA_SIM_LDFLAGS)

CLOCK_PERIOD ?= 1.0
RESET_DELAY ?= 777.7

SIM_PREPROC_DEFINES = \
	+define+CLOCK_PERIOD=$(CLOCK_PERIOD) \
	+define+RESET_DELAY=$(RESET_DELAY) \
	+define+PRINTF_COND=$(TB).printf_cond \
	+define+STOP_COND=!$(TB).reset \
	+define+MODEL=$(MODEL) \
	+define+RANDOMIZE_MEM_INIT \
	+define+RANDOMIZE_REG_INIT \
	+define+RANDOMIZE_GARBAGE_ASSIGN \
	+define+RANDOMIZE_INVALID_ASSIGN
